
var async = require('async')



module.exports = function (app, passport, auth) {

var index = require('../app/controllers/index');
var users = require('../app/controllers/users');


    
app.get('/signin', users.signin);
app.get('/signup', users.signup);
app.get('/details',users.fbDetails);
app.get('/test',users.test);
app.get('/pages',users.showPage);
app.get('/groups',users.showGroup);
app.get('/postArticle',users.postArticle);
app.get('/signout',users.signout);
app.get('/users/facebookSignup',users.facebookSignup);
app.get('/auth/facebook', passport.authenticate('facebook', { scope: [ 'email', 'user_about_me', 'read_stream', 'publish_actions', 'user_friends', 'manage_pages', 'user_groups','publish_stream','manage_notifications','read_friendlists','read_mailbox','read_stream','user_photos'], failureRedirect: '/' }));
app.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/' }), users.authCallback);
app.get('/', index.render);
app.get('/other',users.other);
app.get('/picture',users.picture);
app.get('/albums',users.album);
app.get('/notification',users.notification);

app.post('/users/session', passport.authenticate('local', {failureRedirect: '/#loginError', failureFlash: 'Invalid email or password.'}), users.home);
app.post('/submitArticle',users.submitArticle);




}
