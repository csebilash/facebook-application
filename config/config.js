
var path = require('path')
  , rootPath = path.normalize(__dirname + '/..')

module.exports = {
  development: {
    db: 'mongodb://localhost/facebook',
    root: rootPath,
    app: {
      name: 'Facebook'
    },
    facebook: {
     clientID: "366830853419313",
     clientSecret: "215cb3e51aed7e9b64a81231a69763e4",
      callbackURL: "http://localhost/auth/facebook/callback"
    }
  
  },


  production: {
    db: 'mongodb://localhost/facebook',
    root: rootPath,
    app: {
      name: 'Facebook'
    },
    facebook: {
      clientID: "APP_ID",
      clientSecret: "APP_SECRET",
      callbackURL: "http://localhost:3000/auth/facebook/callback"
    }
  
  }
}
