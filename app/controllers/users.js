var mongoose = require('mongoose')
  , User = mongoose.model('User')
  , fs = require('fs')
  , _ = require("underscore")
  ,request = require('request')

exports.authCallback = function (req, res, next) {
res.redirect('/details')


}

exports.signin = function (req, res) {
  res.render('signIn', {
    title: 'Signin',
    message: req.flash('error')
  })
}


exports.signup = function (req, res) {
    res.render('signup');
}

exports.fbDetails=function(req,res){
    res.render('details',{
      user:req.user
    });

}



exports.changePassword=function(req,res){
  
  if(req.user.password==req.body.password)
  {

    req.user.password=req.body.new_password;
  }
  req.user.save(function(err){
      if(err)
        console.log(err);
      else
      {
          res.redirect('/userArticle');
      }
      
    });


} 
exports.signout = function (req, res) {
  req.logout()
  res.redirect('/')
}




exports.session = function (req, res) {
  console.log('Session:');
  console.log(req.body);
 /* var url=req.session.redirectUrl || '/';
  req.session.redirectUrl=null;
  console.log(req.session.redirectUrl);
  console.log(url);
  res.redirect(url);  */
  
}

exports.home=function(req,res){
  res.redirect('/userArticle');


}



exports.facebookSignup = function(req,res){

  res.redirect('/auth/facebook');


}

exports.test=function(req,res){

            var url = 'https://graph.facebook.com/me/groups';
            var accessToken = req.user.facebookAccessToken;
            var params = {
                access_token: accessToken,
            };
            request.get({  url: url, qs: params}, function(err, resp, groups) {
               var groups = JSON.parse(groups);
           

              var url = 'https://graph.facebook.com/me/friends';
              var accessToken = req.user.facebookAccessToken;
              var params = {
                  access_token: accessToken,
              }
              request.get({  url: url, qs: params}, function(err, resp, friends) {

                var friend = JSON.parse(friends);
                res.send({groups: groups.data, pages: pages.data, friends:friend});
              })

               
            })



}

exports.picture=function(req,res)
{
    console.log('Picture')
    var url = 'https://graph.facebook.com/me/photos/uploaded';
    var accessToken = req.user.facebookAccessToken;
    var params = {
        access_token: accessToken,
    }
    request.get({  url: url, qs: params}, function(err, resp, photos) {

      var photo = JSON.parse(photos);
      res.render('picture',{
          photos:photo
      });
    });

}

exports.album = function(req,res){
    console.log('Albums');
    var url = 'https://graph.facebook.com/me/albums';
    var accessToken = req.user.facebookAccessToken;
    var params = {
        access_token: accessToken,
    }
    request.get({  url: url, qs: params}, function(err, resp, album) {

      var albums = JSON.parse(album);
     

      res.render('album',{
          album:albums
      });
    });
}

exports.other = function(req,res)
{

    var url = 'https://graph.facebook.com/me/groups';
    var accessToken = req.user.facebookAccessToken;
    var params = {
        access_token: accessToken,
    };
    request.get({  url: url, qs: params}, function(err, resp, groups) {
       var groups = JSON.parse(groups);
   

      var url = 'https://graph.facebook.com/me/friends';
      var accessToken = req.user.facebookAccessToken;
      var params = {
          access_token: accessToken,
      }
      request.get({  url: url, qs: params}, function(err, resp, friends) {

        var friend = JSON.parse(friends);

        
        res.render('friends',{
             friends:friend
        })
      })

               
  })

}


exports.notification = function(req,res){
var url = 'https://graph.facebook.com/me/notifications';
      var accessToken = req.user.facebookAccessToken;
      var params = {
          access_token: accessToken,
      }
      request.get({  url: url, qs: params}, function(err, resp, noti) {

        var notification = JSON.parse(noti);
        
        
        res.render('notification',{
            notifications:notification
        });
      })



  
}


exports.postArticle=function(req,res){
    var pages;
    var url = 'https://graph.facebook.com/me/accounts';
    var accessToken = req.user.facebookAccessToken;
    var params = {
        access_token: accessToken,
    };
    request.get({  url: url, qs: params}, function(err, resp, pages) {
        pages = JSON.parse(pages);
        var url = 'https://graph.facebook.com/me/groups';
        var accessToken = req.user.facebookAccessToken;
        var params = {
            access_token: accessToken,
        };
        request.get({  url: url, qs: params}, function(err, resp, groups) {
        var groups = JSON.parse(groups);
     
          res.render('posted',{
              groups:groups,
              pages:pages
          }); 

        });
    });
}


exports.showPage=function(req,res){
    var pages;
    var url = 'https://graph.facebook.com/me/accounts';
    var accessToken = req.user.facebookAccessToken;
    var params = {
        access_token: accessToken,
    };
    request.get({  url: url, qs: params}, function(err, resp, pages) {
            pages = JSON.parse(pages);

            res.render('page',{
              pages: pages.data
            });

    });


  
}


exports.showGroup=function(req,res)
{
      var url = 'https://graph.facebook.com/me/groups';
      var accessToken = req.user.facebookAccessToken;
      var params = {
          access_token: accessToken,
      };
      request.get({  url: url, qs: params}, function(err, resp, groups) {
         var groups = JSON.parse(groups);
        
        res.render('group',{
          groups: groups.data
        });
       });
}




exports.submitArticle=function(req,res){

  if(req.body.area == 'Profile')
  {
      url = 'https://graph.facebook.com/me/feed';
      var accessToken = req.user.facebookAccessToken;
  }
  else if(req.body.area == 'Page')
  {

      url = 'https://graph.facebook.com/' +req.body.areaID  + '/feed';
      var accessToken = req.body.token;
  }
  else if(req.body.area == 'Group')
  {
      url = 'https://graph.facebook.com/' + req.body.areaID + '/feed';
      var accessToken = req.user.facebookAccessToken;
  }

        //var accessToken = req.user.facebookAccessToken;
        var message=req.body.data;
        
        var params = {
          access_token: accessToken,
          link: 'https://www.bilashcse.com',
          message: message
        };
        request.post({
                url: url, 
                qs: params
            }, function(err, resp, body) {
                // Handle any errors that occur
                if (err){
                    console.error("Error occured: ", err);
                    res.end('Error');
                    return ;
                } 
                body = JSON.parse(body);

                if (body.error){
                    res.end('error occoured');
                    return console.error("Error returned from facebook: ", body.error);
                }
                res.end('sent');
        });
  


}