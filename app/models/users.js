var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , crypto = require('crypto')
  ,uuid = require('node-uuid')
  
  , authTypes = [ 'facebook']
  
  
  var UserSchema = mongoose.Schema({
            
            salt: {type: String, required: true, default: uuid.v1},
            name: String,
            email: String,
            type: String,
            password: String,
            age: String,
            gender: String,
            country: String,
            facebook: {},
            facebookAccessToken: {},





    })
    
var hash = function(passwd, salt) {
  
return crypto.createHmac('sha256', salt).update(passwd).digest('hex');
};


UserSchema.methods.setPassword = function(passwordString) {
    
    this.password = hash(passwordString, this.salt);
};
UserSchema.methods.isValidPassword = function(passwordString) {
    return this.password === hash(passwordString, this.salt);
};

 mongoose.model('User',UserSchema);